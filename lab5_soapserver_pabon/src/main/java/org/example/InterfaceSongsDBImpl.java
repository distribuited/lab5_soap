package org.example;

import org.example.classes.Song;

import javax.jws.WebService;
import java.util.ArrayList;

@WebService(endpointInterface = "org.example.InterfaceSongsDB")
public class InterfaceSongsDBImpl implements InterfaceSongsDB {
    private Song[] listSongs;

    public InterfaceSongsDBImpl(){
        listSongs = new Song[]{
                new Song("Clocks", "ColdPlay"),
                new Song("Fix You", "ColdPlay"),
                new Song("My Universe", "ColdPlay")
        };
    }

    // getSongs method implemented
    public ArrayList<Song> getSongs(String search) {
        ArrayList<Song> returnList = new ArrayList<>();

        for (Song song : listSongs) {
            if (song.getName().toLowerCase().contains(search.toLowerCase())) {
                returnList.add(song);
            }
        }

        /* Song[] returnArray = returnList.toArray(new Song[returnList.size()]); */
        return returnList;
    }
}
