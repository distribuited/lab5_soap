package org.example;

import javax.xml.ws.Endpoint;

public class SongsDBPublisher {

    public static void main(String[] args) {

        // Publishing the SOAP service

        Endpoint.publish("http://localhost:8080/ws/songsdb", new InterfaceSongsDBImpl());

        System.out.println("Starting web services at http://localhost:8080/ws/songsdb");

    }
}
