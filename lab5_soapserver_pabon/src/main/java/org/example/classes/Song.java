package org.example.classes;

import java.io.Serializable;
public class Song implements Serializable {

    private String name;
    private String artist;
    // util para la de-serialización
    private static final long serialVersionUID = 1L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSinger() {
        return artist;
    }

    public void setSinger(String artist) {
        this.artist = artist;
    }

    public Song(String name, String artist) {
        this.name = name;
        this.artist = artist;
    }

    @Override
    public String toString() {
        return "Song [name=" + name + ", artist=" + artist + "]";
    }

}
