package org.example;

import org.example.classes.Song;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;

@WebService
public interface InterfaceSongsDB {
    @WebMethod
    ArrayList<Song> getSongs(@WebParam String name);
}
