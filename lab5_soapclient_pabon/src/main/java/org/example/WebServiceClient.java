package org.example;

import java.util.ArrayList;
import java.util.Scanner;

public class WebServiceClient {
    public static void main(String[] args) {
        try {
            // Creating a service instance for SOAP web service
            InterfaceSongsDBImplService service = new InterfaceSongsDBImplService();
            // Getting the port for the SOAP service
            InterfaceSongsDB songsDB = service.getInterfaceSongsDBImplPort();

            Scanner sc = new Scanner(System.in);
            int numberOption;
            String songName;
            ArrayList<Song> result = new ArrayList<Song>();
            String menu = "\n+++++[Opciones]+++++\n\n[-1] = Salir\n[0] = Buscar canción\nElige: ";

            do {
                System.out.println(menu);
                try {
                    numberOption = Integer.parseInt(sc.nextLine());
                } catch (NumberFormatException e) {
                    numberOption = -1;
                }

                if (numberOption != -1) {

                    System.out.println("Ingresa el nombre de la canción que quieres buscar: ");
                    songName = sc.nextLine();
                    try {
                        // Calling the SOAP service method to get songs
                        result = (ArrayList<Song>) songsDB.getSongs(songName);
                    } catch (Exception e) {
                        System.out.println(e);
                        throw new RuntimeException(e);
                    }
                    for (Song song : result) {
                        System.out.println("Resultados = " + song.toString());
                    }
                    System.out.println("Presiona ENTER para hacer otra búsqueda");
                    sc.nextLine();
                }
            } while (numberOption != -1);
            sc.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}